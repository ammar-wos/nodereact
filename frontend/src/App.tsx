import { FC } from 'react';
import './App.css';
import { MianContent } from './containers';

export const App: FC = () => {
  return <>
    <MianContent />
  </>
}
