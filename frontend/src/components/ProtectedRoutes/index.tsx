import { FC } from "react";
import { Navigate } from "react-router-dom";

type ProtectedRouteProps = {
    id: Number,
    path: string,
    name: string,
    icon: any,
    element: any,
    isVisible: boolean,
    isAuth: boolean,
    accessRoles: Array<string>
}

export const ProtectedRoute: FC<ProtectedRouteProps> = ({
    isAuth,
    isVisible,
    accessRoles,
    element
}) => {
    const isAuthenticated = false;
    return isAuth === isAuthenticated ? element : <Navigate to={"/login"} />
}