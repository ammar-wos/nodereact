import { LoginForm } from "../containers/login/index";

export const routeList = [
    {
        id: 1,
        path: "/login",
        name: "Login",
        icon: "",
        element: <LoginForm />,
        isVisible: false,
        isAuth: false,
        accessRoles: []
    }
];