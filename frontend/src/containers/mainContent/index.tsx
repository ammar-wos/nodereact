import { FC, useState } from "react";
import { BrowserRouter as Router, Navigate, Route, Routes } from "react-router-dom"
import { ProtectedRoute } from "../../components";
import { routeList } from "../../routes";
export const MianContent: FC = () => {
    const [isSidebar, setIsSidebar] = useState<boolean>(false);
    return <>
        <div className="page-root-element">
            <Router>
                <Routes>
                    {routeList.map((routeItem: any) => {
                        return (< Route
                            key={"main" + routeItem.id}
                            path={routeItem.path}
                            element={
                                <ProtectedRoute {...routeItem} />
                            }
                        />)
                    })}
                    <Route path="/" element={<Navigate to={"/login"} />} />
                </Routes>
            </Router>
        </div>
    </>
}