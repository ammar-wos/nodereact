import { FC } from "react"
import { Form, Button, Row, Col } from "react-bootstrap"
import "./login.css"
export const LoginForm: FC = () => {
    return <>
        <div className="main-auth">
            <div className="sub-main-auth">
                <div className="w-100 text-left">
                    <h2 className="auth-title">Login</h2>
                    <Form>
                        <Form.Group as={Row} className="form-input-group" controlId="formBasicEmail">
                            <Form.Label column sm="3">Email : </Form.Label>
                            <Col sm="9">
                                <Form.Control defaultValue="email@example.com" />
                            </Col>
                        </Form.Group>

                        <Form.Group as={Row} className="form-input-group" controlId="formBasicEmail">
                            <Form.Label column sm="3">Password : </Form.Label>
                            <Col sm="9">
                                <Form.Control defaultValue="email@example.com" />
                            </Col>
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </Form>
                </div>
            </div>
        </div>

    </>
}