import { applyMiddleware, compose } from "redux"
import { configureStore } from "@reduxjs/toolkit";
import { routerMiddleware as createRouterMiddleware } from "react-router-redux";
import { createBrowserHistory } from "history"
import { createLogger } from "redux-logger";
import { config } from "./config";
import thunk from "redux-thunk";
import { rootReducer, RootState } from "../reducers";
declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
}
export const history = createBrowserHistory();
export const routerMiddleware = createRouterMiddleware(history);

const logger = createLogger({
    // ...options
});

const middlewares = [routerMiddleware, thunk];
if (config.APP_TYPE === "development") {
    middlewares.push(logger);
}

const composeEnhancer =
    config.APP_TYPE === "development"
        ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
        : compose;

export const createStore = (initialState?: RootState) => {
    return configureStore({
        reducer: rootReducer,
        preloadedState: initialState!,
        enhancers: [],
        middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(middlewares),

    });
};
export const store = createStore();