const express = require('express');
const cors = require('cors');
const { createServer } = require("http");
const { config } = require('./config');
const { routes } = require("./routes");

const app = express();
app.use(cors());
app.use(express.json());

app.use(routes);

const httpServer = createServer(app);
httpServer.listen(config.port, async () => {
    console.log(`server is running`);
});