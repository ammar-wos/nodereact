const { home } = require("./home")
const { Router } = require('express')

const routes = Router();

routes.use("/home", home);

module.exports = {
    routes
}