const { Router } = require('express');
const { createUser } = require('../modules/users');
const home = Router();

home.get('/', createUser)

module.exports = {
    home
}