require('dotenv').config();

const config = process.env.NODE_ENV === "development" ? {
    database: {
        host: process.env.DEVELOPMENT_DB_HOST || "localhost",
        database: process.env.DEVELOPMENT_DB_DATABASE || "mernStack",
        dialect: process.env.DEVELOPMENT_DB_DIALECT || "mysql",
        username: process.env.DEVELOPMENT_DB_USERNAME || "root",
        password: process.env.DEVELOPMENT_DB_PASSWORD || "",
        port: process.env.DEVELOPMENT_DB_PORT || 3306,
    },
    port: process.env.DEV_PORT || 8000
} : {
    database: {
        host: process.env.PRODUCTION_DB_HOST || "localhost",
        database: process.env.PRODUCTION_DB_DATABASE || "mernStack",
        dialect: process.env.PRODUCTION_DB_DIALECT || "mysql",
        username: process.env.PRODUCTION_DB_USERNAME || "root",
        password: process.env.PRODUCTION_DB_PASSWORD || "",
        port: process.env.PRODUCTION_DB_PORT || 3306,
    },
    port: process.env.PROD_PORT || 8000
}

module.exports = {
    config
}