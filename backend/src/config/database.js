const { config } = require('./index')

module.exports = {
    development: config.database,
    test: config.database,
    production: config.database
}
