const { User } = require("../../database/models/index")
const createUser = async (req, res) => {
    const ammar = await User.create({
        firstName: "Ammar",
        lastName: "Shaikh",
        email: "ammar@yopmail.com"
    });

    console.log({ ammar })
    return res.status(200).json({
        status: 1,
        message: "success!",
        data: ammar
    });
}

module.exports = {
    createUser,
}